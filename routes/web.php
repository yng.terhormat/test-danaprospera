<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Route::get('oke', function () {
	return view('index-');
});
Route::get('barang', 'ConItems@index');
Route::get('mutasi', 'ConMutation@index');
Route::get('mutasi/laporan', 'ConMutation@report');


Route::post('api/barang', 'ConApi@barang');
Route::post('api/getbarang', 'ConItems@item_aut');
Route::post('api/mutasi', 'ConApi@mutasi');
Route::post('api/mutasi/autocom', 'ConApi@mutasi');
Route::post('api/mutasi/list', 'ConApi@mutasi');
Route::post('api/itemut', 'ConApi@itemut');
