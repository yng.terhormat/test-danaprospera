<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModCmutationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itemut', function (Blueprint $table) {
            $table->increments('itemutid');
            $table->string('mutationid', 100);
            $table->string('itemid', 100);
            $table->tinyInteger('status')->nullable();
            $table->integer('in')->nullable();
            $table->integer('out')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('childmutation');
    }
}
