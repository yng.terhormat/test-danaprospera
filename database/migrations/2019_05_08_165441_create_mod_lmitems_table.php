<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModLmitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logmutationitem', function (Blueprint $table) {
            $table->increments('id');
            $table->string('itemid', 100);
            $table->integer('initamount')->nullable();
            $table->integer('finalamount')->nullable();
            $table->integer('currentamount')->nullable();
            $table->string('description', 100);
            //$table->timestamps();
            $table->dateTime('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logmutationitem');
    }
}
