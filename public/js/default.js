
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$(document).ready(function() {
		//$("#myModal").modal('show');
	});

	function _json(str) {
		var obj = JSON.parse(str);
		return obj;
	}
	function isJson(text) {
    if (typeof text=="object") return true;
    else return false;
	}
	function _post(path, prm, callback) {
		//$('body').addClass('waiting');
    document.body.style.cursor = 'wait';
		isajax = true; //index.blade.php
		$.ajax({
			//url: host + 'index.php/' + path, //for CI //'mypage.html',
			url: host + '/' + path, //for laravel
			type: "POST",
			data: prm,
			success: function(res) {
				if (callback!='') _parse(callback, res);
				else {
					addalert("child", "Success " + res); //alert('success');
					isajax = false;
				}
				document.body.style.cursor = 'default';
			},
			error: function(err) {
				//$('body').removeClass('waiting');
				addalert("child", "Error " + err); //alert(err);
				isajax = false;
				document.body.style.cursor = 'default';
			}
		});
	}
	/*function _post(path, prm, callback) {
		$('body').addClass('waiting');
		isajax = true; //index.blade.php
		$.ajax({
			url: host + '/' + path, //'mypage.html',
			type: "POST",
			data: prm,
			success: function(res) {
				if (callback!='') _parse(callback, res);
				else {
					addalert("child", "Success " + res); //alert('success');
					isajax = false;
				}
			},
			error: function(err) {
				$('body').removeClass('waiting');
				addalert("child", "Error " + err); //alert(err);
				isajax = false;
			}
		});
	}*/
	function _parse(id, res) {
		var sp = id.split("|");
		$('body').removeClass('waiting');
		switch(sp[0]) {
			case "infomed": //obat.php
				resinfomed(sp[1], res); //master.php
			break;
			case "savesetdef": //setting.php
				ressavesetdef(sp[1], res); //setting.php
			break;
			case "updatesetdef": //setting.php
				resupdatesetdef(sp[1], res); //setting.php
			break;
			case "recipe": //obat.php
				resrecipe(sp[1], res); //obat.php
			break;
			case "paging": //setting.php
				$('#' + sp[1]).html(res);
			break;
			case "addpatient": //patient.php
				resaddpatient(sp[1], res);
			break;
			case "resback": //pasien.php
				console.log("resback " + sp[1]);
				$('#' + sp[1]).html(res);
			break;
			case "updateusers": //user_c.php
				resupdateusers(sp[1], res); //user_c.php
			break;
			case "savep5x6": //savep5x6.php
				ressavep5x6(sp[1], res); //savep5x6.php
			break; 
			case "savemut": //report.blade.php
				ressavemut(sp[1], res); //report.blade.php
			break; 
			case "itemut": //mutation.blade.php
				resitemut(sp[1], res); //mutation.blade.php
			break;
			case "noreload": //mutation.blade.php
				resnoreload(sp[1], res); //mutation.blade.php
			break;//
			default:
				//alert(res);
				addalert("child", "Respon unregistered");
		}
	isajax = false;
	}
  function o_paging(obj) {
    //console.log('other paging ' + $(obj).attr('href') + ' ' + $(obj).attr('filter'));
    var c = c_paging('formfind');
    console.log('o_paging ' + c);
    if (c==false) {
      addalert($(obj).attr('filter'), "Harap perika filter");
      c_addalert();
    } else _post($(obj).attr('href'), 'term=1&' + $('#formfind').serialize(), 'paging|' + $(obj).attr('filter'));
    return false;
  }
  function c_paging(id) { 
  	var res = true;
		var x = $("#" + id).serializeArray();
		$.each(x, function(i, field) { //$("#results").append(field.name + ":" + field.value + " ");
			console.log(field.name + ":" + field.value);
			if (field.value=='') {
				res = false;
				return false;
			}
		});

	return res;
  }
	function addalert(id, ket) {
    alert = '<div class="w3-panel w3-blue w3-display-container" name="addalert" id="addalert" >' +
		        '	<span onclick="this.parentElement.style.display=\'none\'" class="w3-button w3-large w3-display-topright">&times;</span>' +
		        '	<h3>Info!</h3>' +
		        '	<p>' + ket + '</p>' +
		        '</div>';
		$("#" + id).prepend(alert);
	}
  function _reload() {
    setTimeout(function() {
      location.reload();
    }, 1000);/**/
  }
  function c_addalert() {
    setTimeout(function() {
    	$('#addalert').css('display', 'none'); //location.reload();
    }, 1000);/**/
  }
  function r_formcetak() {
    setTimeout(function() {
    	console.log('reloa r_formcetak');
    	$('#formcetak').attr('action', '');
    	$('#hidden').val('');
    	$('#additional').val('');
    }, 1000);/**/
  }
  /*function cetak(id, url) {
    var mapForm = document.createElement("form");
    mapForm.target = "Map";
    mapForm.method = "POST"; // or "post" if appropriate
    mapForm.action = url; //'<?=base_url();?>' + url; //"index.php/cetak/medium";

    var mapInput = document.createElement("input");
    mapInput.type = "hidden";
    mapInput.name = "term";
    mapInput.value = id;
    mapForm.appendChild(mapInput);

    document.body.appendChild(mapForm);

    map = window.open("", "Map", "status=0,title=0,height=600,width=800,scrollbars=1");

    if (map) mapForm.submit();
    else {
    	addalert('main', 'You must allow popups for this map to work.'); //alert('You must allow popups for this map to work.');
    	c_addalert();
    }
  }*/
  function cetak(id, url, add) {
    $('#termprint').val(id);	$('#formcetak').attr('action', url);	$('#additional').val(add);
    
    var mapForm = document.getElementById("formcetak");
    map = window.open("","Map","status=0,title=0,height=600,width=800,scrollbars=1");

    if (map) mapForm.submit();
    else {
      addalert('main', 'You must allow popups for this map to work.'); //alert('You must allow popups for this map to work.');
      c_addalert();
    }
    r_formcetak();
  }
	/*function openCity(evt, cityName) {
	  var i, x, tablinks;
	  x = document.getElementsByClassName("city");
	  for (i = 0; i < x.length; i++) x[i].style.display = "none";
	  tablinks = document.getElementsByClassName("tablink");
	  for (i = 0; i < x.length; i++) tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
	  document.getElementById(cityName).style.display = "block";
	  evt.currentTarget.firstElementChild.className += " w3-border-red";

	  childload(cityName);
	}*/
	function openCity(evt, cityName) {
	  var i, x, tablinks;
	  x = document.getElementsByClassName("city");
	  for (i = 0; i < x.length; i++) {
	    x[i].style.display = "none";
	  }
	  tablinks = document.getElementsByClassName("tablink");
	  for (i = 0; i < x.length; i++) {
	    tablinks[i].className = tablinks[i].className.replace(" w3-indigo", "");
	  }
	  document.getElementById(cityName).style.display = "block";
	  evt.currentTarget.className += " w3-indigo";
	  childload(cityName);
	}	
	function childload(id) {
		console.log(id);
		switch(id) {
			case "users": //setting.php
				if ($('#' + id).html()=='') _post('users', 'prm', 'resback|users');
			break;
			case "print5x6": //setting.php
				if ($('#' + id).html()=='') _post('setting/print5x6', 'prm', 'resback|print5x6');
			break;
			/*default:
				//alert(res);
				addalert("child", "Respon unregistered");*/
		}
	}