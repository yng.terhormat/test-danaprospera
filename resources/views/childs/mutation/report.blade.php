<style type="text/css">
  .headmutc td {
    border: none;
  }
  .headmutc {
    border-right: 1px solid grey;
    border-left: 1px solid grey;
    border-bottom: 1px solid grey;
  }
</style>    
    <div id="main" class="main" style="overflow-y: scroll;padding-bottom: 15px;">

        <div class="row">
          <div class="col-12 col-s-12">

            <form name="mutation" id="mutation" method="get">
              <p style="text-align: right; margin: 5px 0px 0px 0px;">
                <input type="text" name="tgla" id="tgla" value="{{ $_tgla }}" size="12">s/d
                <input type="text" name="tglb" id="tglb" value="{{ $_tglb }}" size="12">

                <input type="text" name="keyword" id="keyword" value="" placeholder="Nama Barang">
                <input type="submit" name="addrow" id="addrow" value="Cari" >
              </p>
            </form>

            <table name="tbl-mutation" id="tbl-mutation" rows="1" class="table-adminer" style="width: 100%">
              <thead>
                <tr>
                  <th style="width: 60px;">Delete</th>
                  <th style="width: 100px;">Tanggal</th>
                  <th style="width: %;">No Bukti</th>
                  <th style="width: %;">Barang</th>
                  <th style="width: 60px;">In</th>
                  <th style="width: 60px;">Out</th>
                  <th style="width: 150px;">Saldo</th>
                </tr>
              </thead>
              <tbody>
                @if (isset($data))


                  @foreach ($data as $key => $mut)
                    <form id="formmut{{$key}}">
                      <tr class="headmut" id="row{{ $key }}" name="row[{{ $key }}]" noid="{{ $key }}" >
                        <td>
                          <input type="button" name="del[{{$key}}]" id="del{{$key}}" value="Delete" onclick="delme({{$key}});" >
                        </td>
                        <td>
                          <input type="hidden" name="muttemp[{{$key}}]">
                          <input type="text" class="datemut adminer-input" name="datemut{{ $key }}" id="datemut{{$key}}" value='{{ date("d-m-Y", strtotime($mut->datemut_at)) }}' disabled="disabled" > </td>
                        <td colspan="5" style="cursor: pointer;">
                          <input type="text" class="adminer-input" id="mutationid{{$key}}" name="mutationid{{$key}}" style="width: 150px" value="{{$mut->mutationid}}" disabled="disabled">
                          <!-- {{ count($mut->items) }} -->
                        </td>
                      </tr>

                      @foreach ($mut->items as $index => $item)
                        <tr class="headmutc"  style="display: none;">
                          <td></td>
                          <td></td>
                          <td>
                            <input type="hidden" name="itetemp[{{$index}}]">
                            <input type="hidden" class="adminer-input" id="itemutid{{$key.$index}}" name="itemutid{{$key.$index}}" value="{{$item->pivot->itemutid}}" ></td>
                          <td><input type="text" class="adminer-input" id="itemname{{$key.$index}}" name="itemname{{$key.$index}}" value="{{$item->name}}" disabled="disabled" ></td>
                          <td style="text-align: right;"><input type="text" class="adminer-input" id="itempin{{$key.$index}}" name="itempin{{$key.$index}}" value="{{$item->pivot->in}}" disabled="disabled" ></td>
                          <td style="text-align: right;"><input type="text" class="adminer-input" id="itempout{{$key.$index}}" name="itempout{{$key.$index}}" value="{{$item->pivot->out}}" disabled="disabled" ></td>
                          @if ($index==count($mut->items)-1)
                            <td>
                              <input type="button" name="editme[{{$key.$index}}]" id="editme{{$key.$index}}" value="Edit" onclick="editme('{{$key}}', '{{$index}}', '{{count($mut->items)}}');">
                              <input type="button" name="editme[{{$key.$index}}]" id="editme{{$key.$index}}" value="Cancel" onclick="editme('{{$key}}', '{{$index}}', '{{count($mut->items)}}');">
                            </td>
                          @else 
                            <td></td>
                          @endif
                        </tr>
                      @endforeach
                    </form>

                  @endforeach
                @endif

              </tbody>
            </table>

            <!--<div id="rescus" class="scrollable">
              <div style="text-align: right;">
              </div>
            </div>
            <div>
            <br>
            <?php
              //$add->Pagination($_SERVER['PHP_SELF'], $rCount, $pg, $jml, "jml=$jml&field=$field&isi=$isi");
            ?>
            </div>-->
          </div>

        </div>
        <div class="row">
          <div class="col-6 col-s-6"></div>
          <div class="col-6 col-s-6" style="text-align: right;">
            <!--<input type="text" name="gamount" id="gamount" value="0"><br>
            <input type="text" name="uprice" id="uprice" value="0"><br>
            <input type="text" name="uchange" id="uchange" value="0"><br><br>
            <input type="button" id="pay" name="pay" value="Payment" onclick="check_submit();">-->
            <!--<input type="button" id="save" name="save" value="Simpan" onclick="return cekisian();">
            <input type="button" id="print" name="print" value="Print">-->
          </div>
        </div>

  	</div>
    <script type="text/javascript">
      /*var howuse = '<?=$howtouse;?>'; //[]; //["Aturan Pakai", "3x1", "2x1", "1x1", "4x1/2", "3x1/2", "2x1/2", "1x1/2"];
      howuse = howuse.split("|");
      var howuseket = '<?=$infouse;?>'; //["Informasi Pemakaian", "Sendok Makan", "Sendok Teh", "Bungkus/racikan", "Tabled", "Kapsul"];
      howuseket = howuseket.split("|");
      var introction = '<?=$introuse;?>'; //["Petunjuk", "10 menit", "20 menit", "1 jam", "2 jam"];
      introction = introction.split("|");
      var introctionadd = '<?=$introinfo;?>'; //["Petunjuk Pemakaian", "Sebelum makan", "Sesudah makan"];
      introctionadd = introctionadd.split("|");
      var tipe = ['Indikator', 'Masuk', 'keluar'];*/

      $( document ).ready(function() { //console.log( "ready!" );
        /*var id = $('#tbl-mutation').attr("rows"); //$("#rows").val();
        $("#howtouse").html(toselect(howuse, 'atupak-txt', id));    $("#howtouseket").html(toselect(howuseket, 'atupakpem-txt', id));
        $("#introction").html(toselect(introction, 'pet-txt', id)); $("#introctiontime").html(toselect(introctionadd, 'petpem-txt', id));

        $("#tipe").html(toselect(tipe, 'tipe-txt', id));

        $("#name-txt" + id).select();*/

        $( "#tgla" ).datepicker({ 
          dateFormat: 'dd-mm-yy'
        });
        $( "#tglb" ).datepicker({ //datein
          dateFormat: 'dd-mm-yy'
        });
        $( ".datemut" ).datepicker({ //datein
          dateFormat: 'dd-mm-yy'
        });

        $('.headmut').click(function(){
          $(this).nextUntil('tr.headmut').toggle();
        });        
      });
      function editme(id, ch, c) {
        //console.log(id + ch);
        $('#datemut' + id).attr('disabled', false);
        $('#mutationid' + id).attr('disabled', false);
        for (i = 0; i < c; i++) {
          $('#itemname' + id + i).attr('disabled', false);
          $('#itempin' + id + i).attr('disabled', false);
          $('#itempout' + id + i).attr('disabled', false);
        }
        //$('#editme' + id + ch).attr('disabled', false);
        $('#editme' + id + ch).val('Save');
        $('#editme' + id + ch).attr('onclick', 'saveme(' + id + ');');
      }
      function saveme(id) {
        console.log($('#formmut' + id).serialize());
        var prm = 'keyword=mutupdate&' + $('#formmut' + id).serialize();
        //console.log('cekisian prm ' + prm);
        _post('api/mutasi', prm, 'savemut|' + id);

        //$(this).attr('disabled', false);
        $(this).val('Edit');
        $(this).attr('onclick', 'editme();');

      } //$('#' + id).css('background-color', 'aliceblue');
      function ressavemut(id, res) {
        var json = res;
        if (json.respon.code==4) {
          $('#row' + id).css('background-color', 'aliceblue');
          /*$('#formmut'+id+'  input[type="text"]').each(function() {
            $(this).attr('disabled', true);
            console.log($(this).val());
          });

          $('#tbl-purchase > tbody  > tr').each(function() {
            $(this).remove();
          });*/
        }
      }
      function delme(id) {
        var prm = 'keyword=mutdel&term=' + $('#mutationid' + id).val();
        console.log(prm);
        _post('api/mutasi', prm, 'delmut|' + id);
      }

      /*$( "#oname" ).autocomplete({
        //http://127.0.0.1/default-theme/test/datasource.php?term=ja
        //source: "out-file/test/datasource.php",
        source: function(request, response) {
          jQuery.post("api/patient", {keyword: 'autocom', term: request.term, filter: $('#filter').val()}, response, 'json');
        },
        minLength: 2,
        select: function( event, ui ) {
          var res;
          if ($('#filter').val()=='name') res = ui.item.rmid;
          else res = ui.item.name;

          $("#pid").val(ui.item.id);
          var res = '(' + ui.item.umur + ') - ' + res + ', Jenis Kelamin: <span style="font-weight: bold;">' + gender(ui.item.gender) + '</span><br>Alamat: ' + ui.item.address;
          $('#infopasien').html(res);
          $("#name-txt1").focus();
        }
      });
      function autocom(obj) {
        //console.log($(obj).val());
        $(obj).autocomplete({
          source: function(request, response) {
            jQuery.post("api/getbarang", {term: request.term}, response, 'json');
          },
          minLength: 2,
          select: function( event, ui ) {
            //console.log(ui); //console.log("Selected: " + ui.item.value + " aka " + ui.item.id);
            s_item(ui, $(this).attr("rowsel"));
          }
        });
      }
      function s_item(arr, id) {
        $("#barcode-txt" + id).val(arr.item.barcode);
        $("#itemid-txt" + id).val(arr.item.id);
      }
      function toselect(arr, name, id) {
        if (id > 1) id = id;
        else id = $('#tbl-mutation').attr("rows"); //$("#rows").val();

        res = "<select class='adminer-input' name='" + name + "" + id + "' id='" + name + id + "'>";
        for (i = 0; i < arr.length; i++) {
          if (i==0) res += '<option value="null">' + arr[i] + '</option>';
          //else if (i==1) res += "<option value='" + arr[i] + "' selected='selected'>" + arr[i] + "</option>";
          else res += "<option value='" + i + "'>" + arr[i] + "</option>";
        }
        res += "</select>";
      return res;
      }
      function setdate(obj) {
        $( "#datepicker" ).datepicker();
      }*/
      /*function cekisian() {
        if ($('#pid').val()=='') {
          addalert('main', 'Harap kolom pasien !');
          c_addalert(); //$('#pid').focus();
        } else if (cekrows()) {
          var prm = 'keyword=mutsave&jml=' + $('#tbl-mutation').attr("rows") + '&' + $('#mutation').serialize();
          //console.log('cekisian prm ' + prm);
          _post('api/mutasi', prm, 'recipe|yes');
        }
      }*/
      /*function resrecipe(id, res) { 
        var json = res; //_json(res); //console.log(json);
        if (json.respon.code==4) {
          addalert('main', json.respon.desc);
          c_addalert();
          _reload(); //restart();
        } else addalert('main', json.respon.desc);
      }
      function cekrows() {
        var noid;
        var res = true;
        var i = 1;
        $('#tbl-mutation > tbody  > tr').each(function() {
          noid = $(this).attr("noid");
          //console.log(noid);
          if ($('#barcode-txt' + noid).val()=='' ||

              $('#tipe-txt' + noid).val()=='null' ||
              $('#jumlah-txt' + noid).val()=='0' ||
              $('#atupak-txt' + noid).val()=='null' ||
              $('#atupakpem-txt' + noid).val()=='null' ||
              $('#pet-txt' + noid).val()=='null' ||
              $('#petpem-txt' + noid).val()=='null'
            ) {

            addalert('main', 'Harap periksa baris ke-' + i);
            c_addalert();
            res = false; //break;
            return false;
          }
          i++;
        });
        //console.log($('#mutation').serialize());

      if (res) return true;
      else return false;
      }
      function restart() {
        $('#tbl-mutation > tbody  > tr').each(function() {
          $(this).remove();
        });
        $('#tbl-mutation').attr("rows", 1);
        addrows_();
      }
      function c_cancel(obj) {
        var noid = $(obj).attr('noid');
        var r = confirm("Are you sure to delete " + $("#name-txt" + noid).val() + "!");
        if (r == true) {
          $('#row' + noid).remove();
        }
      }
      function check_barcode(obj, i) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {//alert('You pressed a "enter" key in ' + $(obj).val());
          var prm = 'keyword=infomaster&barcode=' + $(obj).val();//console.log('c_delete ' + prm);
          _post('api/medicine', prm, 'infomed|' + i);
        }
      }
      function addrows_() {
        var rows = $('#tbl-mutation').attr("rows"); //$('#rows').val();

        rows = parseInt(rows) + parseInt(1);
        $('#tbl-mutation').attr("rows", rows); //$('#rows').val(rows);
        
        var row = addrow(rows); //console.log(row);
        $('#tbl-mutation').append(row);

        $( "#expiredate-txt" + rows ).datepicker({ 
          dateFormat: 'dd-mm-yy'
        });
        $('#name-txt' + rows).select(); //$('#barcode-txt' + rows).select();
      }
      function addrow(id) {
        var admin = "'adminer-input'";
        var row = '<tr id="row' + id + '" name="row[' + id + ']" noid="' + id + '">' +
                  ' <td>' +
                  '   <input class="adminer-input" type="text" name="barcode-txt' + id + '" id="barcode-txt' + id + '" value="" oninput="this.className = ' + admin + '" onkeypress="check_barcode(this, ' + id + ');" >' +
                  '   <input type="hidden" name="itemid-txt' + id + '" id="itemid-txt' + id + '">' +
                  '   <input type="hidden" name="forall[' + id + ']" id="forall' + id + '" value="' + id + '">' +
                  ' </td>' +
                  ' <td><input class="adminer-input" type="text" name="name-txt' + id + '" id="name-txt' + id + '" value="" rowsel="' + id + '" oninput="this.className = ' + admin + '" onkeyup="autocom(this);" ></td>' +
                  
                  ' <td>' + toselect(tipe, "tipe-txt", id) + '</td>' +
                  '<td>' +
                  ' <input type="text" size="5" value="0" name="jumlah-txt' + id + '" id="jumlah-txt' + id + '" style="text-align: center;" onclick="select();">' +
                  '</td>' +
                  ' <td><input type="button" id="del' + id + '" name="del' + id + '" value="Batal" onclick="c_cancel(this);" noid="' + id + '" ></td>' +
                  '</tr>';
      return row;
      }
      function expdate() {
        var currdate = new Date();
        currdate.setDate(currdate.getDate() + 3);
        var hh = '0' + currdate.getDate();
        var mm = '0' + (currdate.getMonth() + 1);
        var formatted_date = hh.substring(hh.length - 2, hh.length) + "-" + mm.substring(mm.length - 2, mm.length) + "-" + currdate.getFullYear()
        //console.log(formatted_date);
      return formatted_date;
      }
      function resinfomed(id, res) {
        obj = _json(res); //console.log(id + '-' +res);
        $('#name-txt' + id).val(obj.name);  $('#quantity-txt' + id).val('1');     $('#quantity-txt' + id).select();
        $('#unit-txt' + id).val(obj.unit);  $('#price-txt' + id).val(obj.price);  $('#idm' + id).val(obj.medicineid);
      }
      function check_total() {
        var gamount = 0;
        $('#tbl-mutation').find('tr').each(function(i, el) { //console.log(i + '-' + el);
          if (i > 0) gamount = parseInt(gamount) + parseInt($('#amount-txt' + i).val() );
        });
        $('#gamount').val(gamount);
      }
      function check_submit() {
        $('#mutation').submit();
      }*/
    </script>