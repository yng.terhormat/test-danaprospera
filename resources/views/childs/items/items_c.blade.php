<?php
$field = ($filter['field']!="NULL") ? $filter['field'] : "";
$term = ($filter['term']!="NULL") ? $filter['term'] : "";
?>

          <p style="margin: 0px 0px;">
            <form id="formfind" name="formfind" style="text-align: right;">
              <select name="field" id="field">
                @if ($field=="")
                <option value="" selected="selected">Cari Berdasarkan</option>
                @else 
                <option value="">Cari Berdasarkan</option>
                @endif

                @if ($field=="itemid")
                <option value="itemid" selected="selected">No Barang</option>
                @else 
                <option value="itemid">No Barang</option>
                @endif

                @if ($field=="name")
                <option value="name" selected="selected">Nama Barang</option>
                @else 
                <option value="name">Nama Barang</option>
                @endif
              </select>
              <input type="text" name="term" id="term" value="{{ $term }}">
              <input type="button" name="find" id="find" value="Cari" ><!-- onclick="addrows_();" -->
            </form>
          </p>
          <table id="tbl-patient" class="table-adminer" style="width: 100%">
            <thead>
              <tr>
                <th style="width: 48px;">Edit</th>
                <th style="width: %;">No Barang</th>
                <th>Nama</th>
                <th style="width: 75px;">Jumlah</th>
                <th style="width: 75px;">Hapus</th>
              </tr>
            </thead>
            <tbody>

              @if(isset($data))
                @foreach ($data as $key => $r)
                  <tr>
                    <td>
                      <!--<input type="button" id="p-edit<?=$key;?>" name="p-edit[<?=$key;?>]" value="Edit" onclick="openedit(this, <?=$r->patientid;?>, '<?=$r->name;?>', '<?//=date("d-m-Y", strtotime($r->birthday));?>', '<?=$r->gender;?>', '<?=$r->address;?>', <?=$r->rmid;?>, '<?=$r->guarantor;?>');" >-->
                      <input type="button" id="p-edit{{ $key }}" name="p-edit[{{ $key }}]" value="Edit" onclick="openedit(this, '{{ $r->itemid }}', '{{ $r->name }}', '{{ $r->quantity }}');" >
                    </td>
                    <td>{{ $r->itemid }}</td>
                    <td>
                      <?php
                      //21
                      if (strlen($r->name) > 12) {
                        $name = "<div class=\"w3-tooltip\">".substr($r->name, 0, 21)."<span class=\"w3-text\">".substr($r->name, 21, strlen($r->name))."</span>";
                      } else $name = $r->name;                  
                      ?>
                      <?=$name;?>
                    </td>
                    <td>{{ $r->quantity }}</td>
                      <?php
                      //21
                      //if (strlen($r->address) > 12) {
                        //$address = "<div class=\"w3-tooltip\">".substr($r->address, 0, 48)."<span class=\"w3-text\">".substr($r->address, 48, strlen($r->address))."</span>";
                      //} else $address = $r->address;                  
                      ?>
                      <?//=$address;?>
                    <td style="text-align: center;">
                      <input type="button" id="p-delete{{ $key }}" name="p-delete[{{ $key }}]" value="Hapus" onclick="i_delete('{{ $r->itemid }}');" >
                    </td>
                  </tr>
                @endforeach
              @endif

            </tbody>
          </table>
          <br>
          <div class="paging-oke">{{ $data->links() }}</div>
          <!-- <div class="paging"></div> -->

          <script type="text/javascript">
            
            $( document ).ready(function() {

              $('#find').click(function () { //itemarea term=1&filter=address&keyword=
                if ($('#field').val()=='') {
                  addalert('itemarea', "Harap pilih filter !");
                  c_addalert();
                  $('#field').focus();
                } else if ($('#term').val()=='') {
                  addalert('itemarea', "Keyword harap diisi !");
                  c_addalert();
                  $('#term').focus();
                } else {
                  var prm = "keyword=itemsearch&";
                  console.log(prm + $('#formfind').serialize());
                  _post('api/barang', prm + $('#formfind').serialize(), 'resback|itemarea');
                  console.log('kirim');
                }
              });
            });
            function i_delete(id) {
              var prm = 'keyword=itemdelete&term=' + id; //+ '&' + $('#item').serialize();
              //console.log('edit patient ' + prm);
              _post('api/barang', prm, 'addpatient|yesdelete'); 
            }
          </script>