    <div id="main" class="main" style="overflow-y: scroll;padding-bottom: 15px;">


      <form name="mutation" id="mutation" method="post">
        <div class="row">
          <div class="col-12 col-s-12">
            <!--<input type="hidden" name="rows" id="rows" value="1">-->

            <!-- <p style="margin: 0px 0px 1px 0px;font-style: italic;">
              <select name="filter" id="filter">
                <option value="name">Nama</option>
                <option value="rmid">No Rekam Medis</option>
              </select>
              <input class="adminer-input" type="text" name="oname" id="oname" onclick="this.select();" style="width: 200px"><br>
            
              <input type="hidden" name="pid" id="pid">            
            </p>
            <div id="infopasien" style="font-style: italic;height: 35px;"></div> -->

            <p style="text-align: right; margin: 5px 0px 0px 0px;">
              <input type="text" name="datein" id="datein" value="<?=date("d-m-Y");?>" size="12"><!---->
              No Resep: <input type="text" name="nomutasi" id="nomutasi" value="<?=$norecipe;?>">
              <input type="button" name="addrow" id="addrow" value="Tambah Barang" onclick="addrows_();" >
            </p>
            <table name="tbl-mutation" id="tbl-mutation" rows="1" class="table-adminer" style="width: 100%">
              <thead>
                <tr>
                  <th style="width: 200px;">Kode Barang</th>
                  <th>Nama</th>
                  <th>Tipe</th>
                  <th style="width: 60px;">Jumlah</th>

                  <!-- <th style="width: 120px;">Aturan Pakai</th>
                  <th style="width: 170px;">Info Pakai</th>
                  <th style="width: 95px;">Petunjuk</th>
                  <th style="width: 165px;">Info Petunjuk</th>
                  <th style="width: 90px;">Kadaluarsa</th> -->

                  <th style="width: 60px;">Batal</th>
                </tr>
              </thead>
              <tbody>
                <tr id="row1" name="row[1]" noid="1">
                  <td>
                    <input class="adminer-input" type="text" name="barcode-txt1" id="barcode-txt1" oninput="this.className = 'adminer-input'" onkeypress="check_barcode(this, 1);" >
                    <input type="hidden" name="itemid-txt1" id="itemid-txt1">
                    <input type="hidden" name="forall[1]" id="forall1" value="1">
                  </td>
                  <td><input class="adminer-input" type="text" name="name-txt1" id="name-txt1" rowsel="1" oninput="this.className = 'adminer-input'" onkeyup="autocom(this);" ></td>
                  <td id="tipe"></td>
                  <td>
                    <input type="text" size="5" value="0" name="jumlah-txt1" id="jumlah-txt1" style="text-align: center;">
                  </td>

                  <!-- <td id="howtouse"></td>
                  <td id="howtouseket"></td>
                  <td id="introction"></td>
                  <td id="introctiontime"></td>
                  <?php
                  //$expdate = mktime(0, 0, 0, date("m"), date("d")+3, date("Y"));
                  ?>
                  <td><input class="setdate adminer-input" type="text" name="expiredate-txt1" id="expiredate-txt1" rowsel="1" oninput="this.className = 'adminer-input'" value="" onclick="setdate(this);" ></td> -->
                  <td><input type="button" name="del1" id="del1" value="Batal" onclick="c_cancel(this);" noid="1" ></td>
                </tr>
                <?//=$tbl;?>
              </tbody>
            </table>

            <!--<div id="rescus" class="scrollable">
              <div style="text-align: right;">
              </div>
            </div>
            <div>
            <br>
            <?php
              //$add->Pagination($_SERVER['PHP_SELF'], $rCount, $pg, $jml, "jml=$jml&field=$field&isi=$isi");
            ?>
            </div>-->
          </div>

        </div>
        <div class="row">
          <div class="col-6 col-s-6"></div>
          <div class="col-6 col-s-6" style="text-align: right;">
            <!--<input type="text" name="gamount" id="gamount" value="0"><br>
            <input type="text" name="uprice" id="uprice" value="0"><br>
            <input type="text" name="uchange" id="uchange" value="0"><br><br>
            <input type="button" id="pay" name="pay" value="Payment" onclick="check_submit();">-->
            <input type="button" id="save" name="save" value="Simpan" onclick="return cekisian();">
            <!--<input type="button" id="print" name="print" value="Print">-->
          </div>
        </div>

      </form>
  	</div>
    <script type="text/javascript">
      var howuse = '<?=$howtouse;?>'; //[]; //["Aturan Pakai", "3x1", "2x1", "1x1", "4x1/2", "3x1/2", "2x1/2", "1x1/2"];
      howuse = howuse.split("|");
      var howuseket = '<?=$infouse;?>'; //["Informasi Pemakaian", "Sendok Makan", "Sendok Teh", "Bungkus/racikan", "Tabled", "Kapsul"];
      howuseket = howuseket.split("|");
      var introction = '<?=$introuse;?>'; //["Petunjuk", "10 menit", "20 menit", "1 jam", "2 jam"];
      introction = introction.split("|");
      var introctionadd = '<?=$introinfo;?>'; //["Petunjuk Pemakaian", "Sebelum makan", "Sesudah makan"];
      introctionadd = introctionadd.split("|");
      var tipe = ['Indikator', 'Masuk', 'keluar'];

      $( document ).ready(function() { //console.log( "ready!" );
        var id = $('#tbl-mutation').attr("rows"); //$("#rows").val();
        $("#howtouse").html(toselect(howuse, 'atupak-txt', id));    $("#howtouseket").html(toselect(howuseket, 'atupakpem-txt', id));
        $("#introction").html(toselect(introction, 'pet-txt', id)); $("#introctiontime").html(toselect(introctionadd, 'petpem-txt', id));

        $("#tipe").html(toselect(tipe, 'tipe-txt', id));

        $("#name-txt" + id).select();

        $( ".setdate" ).datepicker({ 
          dateFormat: 'dd-mm-yy'
        });
        $( "#datein" ).datepicker({ //datein
          dateFormat: 'dd-mm-yy'
        });
      });
      $( "#oname" ).autocomplete({
        //http://127.0.0.1/default-theme/test/datasource.php?term=ja
        //source: "out-file/test/datasource.php",
        source: function(request, response) {
          jQuery.post("api/patient", {keyword: 'autocom', term: request.term, filter: $('#filter').val()}, response, 'json');
        },
        minLength: 2,
        select: function( event, ui ) {

          /*$("#pname").html('(umur)' + ' - ' + ui.item.rmid);
          $("#pket").html(ui.item.address);
          console.log($('#filter').val());
          switch($('#filter').val()) {
            case "name":
              res = ui.item.rmid;
            break;
            case "rmid":
              res = ui.item.name;
            break;
          }*/
          var res;
          if ($('#filter').val()=='name') res = ui.item.rmid;
          else res = ui.item.name;

          $("#pid").val(ui.item.id);
          var res = '(' + ui.item.umur + ') - ' + res + ', Jenis Kelamin: <span style="font-weight: bold;">' + gender(ui.item.gender) + '</span><br>Alamat: ' + ui.item.address;
          $('#infopasien').html(res);
          $("#name-txt1").focus();
        }
      });
      function autocom(obj) {
        //console.log($(obj).val());
        $(obj).autocomplete({
          source: function(request, response) {
            jQuery.post("api/getbarang", {term: request.term}, response, 'json');
          },
          minLength: 2,
          select: function( event, ui ) {
            //console.log(ui); //console.log("Selected: " + ui.item.value + " aka " + ui.item.id);
            s_item(ui, $(this).attr("rowsel"));
          }
        });
      }
      function s_item(arr, id) {
        $("#barcode-txt" + id).val(arr.item.barcode);
        $("#itemid-txt" + id).val(arr.item.id);
      }
      function toselect(arr, name, id) {
        if (id > 1) id = id;
        else id = $('#tbl-mutation').attr("rows"); //$("#rows").val();

        res = "<select class='adminer-input' name='" + name + "" + id + "' id='" + name + id + "'>";
        for (i = 0; i < arr.length; i++) {
          if (i==0) res += '<option value="null">' + arr[i] + '</option>';
          //else if (i==1) res += "<option value='" + arr[i] + "' selected='selected'>" + arr[i] + "</option>";
          else res += "<option value='" + i + "'>" + arr[i] + "</option>";
        }
        res += "</select>";
      return res;
      }
      function setdate(obj) {
        $( "#datepicker" ).datepicker();
      }
      function cekisian() {
        /*if ($('#pid').val()=='') {
          addalert('main', 'Harap kolom pasien !');
          c_addalert(); //$('#pid').focus();
        } else*/ if (cekrows()) {
          var prm = 'keyword=mutsave&jml=' + $('#tbl-mutation').attr("rows") + '&' + $('#mutation').serialize();
          //console.log('cekisian prm ' + prm);
          _post('api/mutasi', prm, 'recipe|yes');
        }
      }
      function resrecipe(id, res) {
        var json = _json(res); //console.log(json);
        if (json.respon.code==4) {
          restart();
          //$('#noroom').val('');
          $('#nomutasi').val(json.respon.norecipe);
          //cetak(json.respon.id, '{{ URL::to('/') }}/index.php/cetak/multi');
          _reload();

        } //$('#' + id).css('background-color', 'aliceblue');

      }
      function cekrows() {
        var noid;
        var res = true;
        var i = 1;
        $('#tbl-mutation > tbody  > tr').each(function() {
          noid = $(this).attr("noid");
          /*console.log(noid);*/
          if ($('#barcode-txt' + noid).val()=='' ||

              $('#tipe-txt' + noid).val()=='null' ||
              $('#jumlah-txt' + noid).val()=='0'
                                                 /*||
              $('#atupak-txt' + noid).val()=='null' ||
              $('#atupakpem-txt' + noid).val()=='null' ||
              $('#pet-txt' + noid).val()=='null' ||
              $('#petpem-txt' + noid).val()=='null'*/
            ) {

            addalert('main', 'Harap periksa baris ke-' + i);
            c_addalert();
            res = false; //break;
            return false;
          }
          i++;
        });
        //console.log($('#mutation').serialize());

      if (res) return true;
      else return false;
      }
      function restart() {
        $('#tbl-mutation > tbody  > tr').each(function() {
          $(this).remove();
        });
        $('#tbl-mutation').attr("rows", 1);
        addrows_();
      }
      function c_cancel(obj) {
        var noid = $(obj).attr('noid');
        var r = confirm("Are you sure to delete " + $("#name-txt" + noid).val() + "!");
        if (r == true) {
          $('#row' + noid).remove();
        }
      }
      function check_barcode(obj, i) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {//alert('You pressed a "enter" key in ' + $(obj).val());
          var prm = 'keyword=infomaster&barcode=' + $(obj).val();//console.log('c_delete ' + prm);
          _post('api/medicine', prm, 'infomed|' + i);
        }
      }
      function addrows_() {
        var rows = $('#tbl-mutation').attr("rows"); //$('#rows').val();

        rows = parseInt(rows) + parseInt(1);
        $('#tbl-mutation').attr("rows", rows); //$('#rows').val(rows);
        
        var row = addrow(rows); //console.log(row);
        $('#tbl-mutation').append(row);

        $( "#expiredate-txt" + rows ).datepicker({ 
          dateFormat: 'dd-mm-yy'
        });
        $('#name-txt' + rows).select(); //$('#barcode-txt' + rows).select();
      }
      function addrow(id) {
        var admin = "'adminer-input'";
        var row = '<tr id="row' + id + '" name="row[' + id + ']" noid="' + id + '">' +
                  ' <td>' +
                  '   <input class="adminer-input" type="text" name="barcode-txt' + id + '" id="barcode-txt' + id + '" value="" oninput="this.className = ' + admin + '" onkeypress="check_barcode(this, ' + id + ');" >' +
                  '   <input type="hidden" name="itemid-txt' + id + '" id="itemid-txt' + id + '">' +
                  '   <input type="hidden" name="forall[' + id + ']" id="forall' + id + '" value="' + id + '">' +
                  ' </td>' +
                  ' <td><input class="adminer-input" type="text" name="name-txt' + id + '" id="name-txt' + id + '" value="" rowsel="' + id + '" oninput="this.className = ' + admin + '" onkeyup="autocom(this);" ></td>' +
                  
                  ' <td>' + toselect(tipe, "tipe-txt", id) + '</td>' +
                  '<td>' +
                  ' <input type="text" size="5" value="0" name="jumlah-txt' + id + '" id="jumlah-txt' + id + '" style="text-align: center;">' +
                  '</td>' +

                  /*' <td>' + toselect(howuse, "atupak-txt", id) + '</td>' +
                  ' <td>' + toselect(howuseket, "atupakpem-txt", id) + '</td>' +
                  ' <td>' + toselect(introction, "pet-txt", id) + '</td>' +
                  ' <td>' + toselect(introctionadd, "petpem-txt", id) + '</td>' + 
                  ' <td><input class="setdate adminer-input" type="text" name="expiredate-txt' + id + '" id="expiredate-txt' + id + '" value="" rowsel="' + id + '" oninput="this.className = ' + admin + '" ></td>' +*/ //expiredate ' + expdate() + '
                  ' <td><input type="button" id="del' + id + '" name="del' + id + '" value="Batal" onclick="c_cancel(this);" noid="' + id + '" ></td>' +
                  '</tr>';
      return row;
      }
      /*function expdate() {
        var current_datetime = new Date();
        var formatted_date = current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear()
        console.log(formatted_date);
      return formatted_date;
      }*/
      function expdate() {
        var currdate = new Date();
        currdate.setDate(currdate.getDate() + 3);
        var hh = '0' + currdate.getDate();
        var mm = '0' + (currdate.getMonth() + 1);
        var formatted_date = hh.substring(hh.length - 2, hh.length) + "-" + mm.substring(mm.length - 2, mm.length) + "-" + currdate.getFullYear()
        //console.log(formatted_date);
      return formatted_date;
      }
      function resinfomed(id, res) {
        obj = _json(res); //console.log(id + '-' +res);
        $('#name-txt' + id).val(obj.name);  $('#quantity-txt' + id).val('1');     $('#quantity-txt' + id).select();
        $('#unit-txt' + id).val(obj.unit);  $('#price-txt' + id).val(obj.price);  $('#idm' + id).val(obj.medicineid);
      }
      function check_total() {
        var gamount = 0;
        $('#tbl-mutation').find('tr').each(function(i, el) { //console.log(i + '-' + el);
          if (i > 0) gamount = parseInt(gamount) + parseInt($('#amount-txt' + i).val() );
        });
        $('#gamount').val(gamount);
      }
      function check_submit() {
        $('#mutation').submit();
      }
    </script>