    <div id="main" class="main" style="overflow-y: scroll;padding-bottom: 15px;">

      <!--<div class="row">
        <div class="col-3 w3-col s12 m6 l6" style="padding-bottom: 0px">
          <button onclick="cetaksmall();">Cetak</button>
        </div>
      </div>-->

      <div class="row">

        <form id="item" name="item" tglnow="<?=date("d-m-Y");?>">
          <div class="col-3 w3-col s12 m12 l4" style="padding-bottom: 0px">
            <label class="w3-text-blue">No Barang:</label>
            <input type="text" class="step-input w3-border" name="p-id" id="p-id" style="margin-bottom: 5px;">
            <label class="w3-text-blue">Nama Barang:</label>
            <input type="text" class="step-input w3-border" name="p-name" id="p-name" filter="name" style="margin-bottom: 5px;">
            <label class="w3-text-blue">Jumlah:</label>
            <input type="text" class="step-input w3-border" name="p-quantity" id="p-quantity" value="0" style="margin-bottom: 5px;">  <!-- disabled="disabled" -->
            
            <!-- <label class="w3-text-blue">Tanggal Lahir:</label>
            <input type="text" class="step-input w3-border" name="p-birthday" id="p-birthday" value="<?=date("d-m-Y");?>" style="margin-bottom: 5px;">
            <label class="w3-text-blue">Jenis Kelamin:</label>
            <select class="step-input w3-border" name="p-gender" id="p-gender" style="margin-bottom: 5px;">
              <option value="">Jenis Kelamin</option>
              <option value="P">Perempuan</option>
              <option value="L">Laki - laki</option>
            </select>
            <label class="w3-text-blue">Penjamin:</label>
            <select class="step-input w3-border" name="p-guarantor" id="p-guarantor" style="margin-bottom: 5px;">
              <option value="">Penjamin</option> -->
              <?php

                /*if (isset($guarantor)) {
                  //$fungsi->extra($quarantor);
                  foreach ($guarantor as $key => $r) {
                    echo "<option value=\"$r->information\">$r->information</option>";
                  }
                }*/
              ?>
              <!--<option value="P">Perempuan</option>
              <option value="L">Laki - laki</option>
            </select>

            <label class="w3-text-blue">No Rekam Medis:</label>
            <input type="text" class="step-input w3-border" name="p-rmid" id="p-rmid" filter="rmid" style="margin-bottom: 10px;">
            -->



            <input type="button" name="addnewpas" id="addnewpas" exist="false" value="Tambah Barang">
            <input type="button" name="cetak-sma" id="cetak-sma" onclick="go_print();" backup="cancel(this);" value="Cetak">
            <!-- <button ></button>
            <button ></button> -->
          </div>
        </form>

        <?php
        //if ($this->session->userdata('user')=="master") {
        ?>
        <div id="itemarea" name="itemarea" class="col-3 w3-col s12 m12 l8" style="padding-bottom: 0px">
          @if (isset($item_c))
          {!! $item_c !!}
          @endif
          <?php
          //if (isset($item_c)) echo $item_c;
          ?>
        </div>
        <?php
        //}
        ?>

      </div>
      
      <div class="row">
        <div class="col-3 w3-col s12 m4 l2" style="padding-bottom: 0px">
        </div>
      </div>
  	</div>
    <script type="text/javascript">

      $( document ).ready(function() {

      var tgl = new Date();
      var b_url = '{{ URL::to('/') }}' + '/';
        $( "#p-birthday" ).datepicker({ 
          dateFormat: 'dd-mm-yy',
          changeYear: true,
          yearRange: (tgl.getFullYear() - 80) + ':' + tgl.getFullYear()
        });
        /*$( "#p-name" ).autocomplete({
          //http://127.0.0.1/default-theme/test/datasource.php?term=ja
          //source: "out-file/test/datasource.php",
          source: function(request, response) {
            //jQuery.post("api/patient", {term: request.term}, response, 'json');
            jQuery.post(b_url + 'index.php/api/patient', {keyword: 'autocom', term: request.term, filter: 'name'}, response, 'json');
          },
          minLength: 2,
          select: function( event, ui ) {
            //console.log(ui);//$('#firstname').val(ui.item.id);
            s_item(ui, 'name');
          }
        });
        $( "#p-rmid" ).autocomplete({
          source: function(request, response) {
            jQuery.post(b_url + 'index.php/api/patient', {keyword: 'autocom', term: request.term, filter: 'rmid'}, response, 'json');
          },
          minLength: 2,
          select: function( event, ui ) {
            //console.log(ui);
            s_item(ui, 'rmid');
          }
        });*/

        var filterform = 'Barang';
        $('#addnewpas').click(function () {
          var button = $(this).val();
          if (button=='Tambah ' + filterform) {
            var prm = 'keyword=itemsave&' + $('#item').serialize();
            console.log($(this).val());
            if (checkinput()) {
              //console.log('patient ' + prm);
              _post('api/barang', prm, 'addpatient|yessave');
            }            
          } else if (button=='Edit ' + filterform) {
            var prm = 'keyword=itemupdate&term=' + $('#item').attr('p-id') + '&' + $('#item').serialize();
            //console.log('edit patient ' + prm);
            _post('api/barang', prm, 'addpatient|yesupdate'); //cetak-sma

          } else if (button=='Cancel') restart();
        });
      });
      function checkinput() {
        var res = true;
        if ($('#p-id').val()=='') {
          addalert('item', "No barang harap diisi !");
          res = false;
          $('#p-id').focus();
        } else if ($('#p-name').val()=='') {
          addalert('item', "Nama barang tidak boleh kosong !");
          res = false;
          $('#p-name').focus();
        }
        if (res==false) c_addalert();

      return res;
      }
      //openedit(0000000001, 'Test Name', '18-01-2019', 'L', 'Jl. Mataram', 00019041901);
      //function openedit(obj, itemid, name, birthday, gender, address, rmid, guarantor) {
      //onclick="openedit(this, Item08052019184345, 'Nama4345', '50');"
      function openedit(obj, itemid, name, quantity) {
        
        roling(1);
        $('#item').attr('p-id', itemid);
        $('#p-id').val(itemid);
        $('#p-name').val(name);
        $('#p-quantity').val(quantity);

        /*$('#p-birthday').val(birthday);
        $('#p-gender').val(gender);
        $('#p-address').val(address);guarantor
        $('#p-rmid').val(rmid);
        $('#p-guarantor').val(guarantor);*/

      }
      function resaddpatient(id, res) {
        var json = res; //_json(res); //console.log(json);
        var pname = $('#p-name').val();
        var pid = $('#p-id').val();

        if (json.respon.code==4) {
          restart();
          if (id=='yessave') {
            //cetak(json.respon.id, 'http://127.0.0.1/sumbu-media/index.php/cetak/small');
            //_post('patient', 'term=1', 'resback|itemarea');

            addalert('item', "Tambah Barang " + pname + "(" + json.respon.id + ") sukses !");
            c_addalert();
            _reload();
          } else if (id=='yesupdate') {
            addalert('item', "Data Barang " + pname + "(" + json.respon.id + ") telah dirubah !");
            c_addalert();
          } else if (id=='yesdelete') {
            addalert('item', "Barang " + pname + "(" + json.respon.id + ") dihapus !");
            c_addalert();
            _reload();
          }

        } else addalert('item', "Data Barang " + pname + "(" + pid + "), tidak dapat dirubah. " + json.respon.desc + " !");//$('#' + id).css('background-color', 'aliceblue');
      }
      function roling(i) {
        if (i==1) {
          $('#cetak-sma').val('Cancel');
          $('#addnewpas').val('Edit Barang');
        } else {
          $('#cetak-sma').val('Cetak');
          $('#addnewpas').val('Tambah Barang');
        }

        var onc = $('#cetak-sma').attr('onclick');
        $('#cetak-sma').attr('onclick', $('#cetak-sma').attr('backup')); /*'cancel(this);'*/
        $('#cetak-sma').attr('backup', onc);
      }
      function restart() {
        roling(2);

        /*$('#p-address').val('');
        $('#p-gender').val('');
        $('#p-rmid').val('');
        $('#p-birthday').val( $('#item').attr('tglnow') );*/

        $('#addnewpas').attr('exist', false);

        $('#item').attr('p-id', "");
        $('#p-id').val('');
        $('#p-name').val('');
        $('#p-quantity').val('');


        /*$('#item').attr('p-id', itemid);
        $('#p-id').val(itemid);
        $('#p-name').val(name);
        $('#p-quantity').val(quantity);*/

      }
      function cancel(obj) {
        restart();
      }
      function go_print() {
        /*var pid = $('#item').attr('p-id');
        var exist = $('#addnewpas').attr('exist');
        var additional = [$('#p-name').val(), $('#p-address').val(), $('#p-birthday').val(), 
        $('#p-gender').val(), $('#p-guarantor').val(), $('#p-rmid').val()];
        //additional.join(" and ");

        if (pid && exist) console.log('tidak terjadi apa-apa'); //cetak(pid, '{{ URL::to('/') }}/index.php/cetak/small', additional.join("|"));
        else {
          addalert('item', "Harap periksa form pasien !");
          c_addalert();
        }*/
      }
      function s_item(ui, filter) {
        if (filter=='name') $('#p-rmid').val(ui.item.rmid);
        else $('#p-name').val(ui.item.name);
        
        $('#item').attr('p-id', ui.item.id);
        $('#addnewpas').attr('exist', true);
        $('#p-address').val(ui.item.address);
        $('#p-birthday').val(ui.item.birthday);
        $('#p-gender').val(ui.item.gender);
        $('#p-guarantor').val(ui.item.guarantor);
        $('#addnewpas').val('Cancel');
      }      
    </script>