
<?php
/*$group = array();
$group['cs'][] = 'patient';

$group['obat'][] = 'medicine';

$group['admin'][] = 'patient';
$group['admin'][] = 'medicine';
$group['admin'][] = 'setting';

$menus = array();
$menus['patient'] = array('url' => 'patient', 'name' => 'Data Pasien');
$menus['medicine'] = array('url' => 'medicine', 'name' => 'Data Resep Obat');
$menus['setting'] = array('url' => 'setting', 'name' => 'Setting');

$menu = array(); //$this->session->userdata('tipe')
$menu[] = "<a href=\"".base_url()."\" class=\"w3-bar-item w3-button w3-border-bottom\">Halaman Depan</a>";
foreach ($group[$this->session->userdata('tipe')] as $key => $r) $menu[] = "<a href=\"".base_url()."index.php/".$menus[$r]['url']."\" class=\"w3-bar-item w3-button w3-border-bottom\">".$menus[$r]['name']."</a>";
$menu[] = "<a href=\"".base_url()."index.php/logout\" class=\"w3-bar-item w3-button w3-border-bottom\">Logout</a>";
*/
?>

<!DOCTYPE html>
<html>
  <head>
    <title>{{ Config::get('app.name') }}</title>
    <!--<title>App</title>-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ URL::to('/') }}/css/responsive-w3school.css" rel="stylesheet">
    <link href="{{ URL::to('/') }}/css/adminer.css" rel="stylesheet">
    <link href="{{ URL::to('/') }}/css/step.css" rel="stylesheet">
    <link href="{{ URL::to('/') }}/css/w3.css" rel="stylesheet">
    <link href="{{ URL::to('/') }}/css/fontawesome-free-5.7.2-web/css/all.min.css" rel="stylesheet">
    <!--<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->

    <link rel="stylesheet" href="{{ URL::to('/') }}/js/jquery-ui-1.12.1/jquery-ui.css">

    <script src="{{ URL::to('/') }}/js/lib/jquery-2.1.3.min.js"></script>
    <script src="{{ URL::to('/') }}/js/jquery-ui-1.12.1/jquery-ui.js"></script>

    <style>
			/**{
				padding: 0;margin: 0;
			}*/
			html, body{
				height: 100%;
			}
      body {
        color: #000;
        background: #fff;
        font: 90%/1.25 Verdana,Arial,Helvetica,sans-serif;
        margin: 0;
      }
			.header{
				min-height: 80px;
				height: 12%;
			}
			.main{
				height: 75%;
			}
			.main-content{
				padding: 20px;
			}
			.footer {
				height: 13%;
			}/**/

      .adminer-input {
        padding: 1px 3px;
      }
      
      ul.pagination .active, .active span  {
        background-color: #0099cc;
        color: white;
        border-radius: 5px;
      }
      ul.pagination .active span {
        padding: 8px 16px;
      }
      ul.pagination {
        display: inline-block;
        padding: 0;
        margin: 0;
      }
      ul.pagination li {
        display: inline;
      }
      ul.pagination li .page-item a {
        float: left;
      }
      ul.pagination li a, span {
        padding: 8px 16px;
        color: black;
        text-decoration: none;
        border-radius: 5px;
      }
      ul.pagination li span {
        text-decoration: none;
        padding: 8px 16px;
      }
      ul.pagination li span:hover, a:hover:not(.active) {background-color: #ddd;}

    </style>
  </head>
  <body>
    <!-- Sidebar -->
    <div class="w3-sidebar w3-bar-block w3-border-right" style="display:none;z-index:5" id="mySidebar">
      <button onclick="w3_close()" class="w3-bar-item w3-button w3-large">Close &times;</button>
      <!--<?php
      //foreach ($menu as $key => $val) echo $val;
      ?>-->
      <a href="{{ URL::to('/') }}" class="w3-bar-item w3-button w3-border-bottom">Halaman Depan</a>
      <a href="{{ URL::to('/') }}/barang" class="w3-bar-item w3-button w3-border-bottom">Data Barang</a>
      <a href="{{ URL::to('/') }}/mutasi" class="w3-bar-item w3-button w3-border-bottom">Mutasi</a>
      <a href="{{ URL::to('/') }}/mutasi/laporan" class="w3-bar-item w3-button w3-border-bottom">Laporan</a>
      <!--<a href="{{ URL::to('/') }}index.php/logout" class="w3-bar-item w3-button w3-border-bottom">Logout</a>-->
    </div>

    <!-- Overlay -->
    <div class="w3-overlay" onclick="w3_close()" style="cursor:pointer" id="myOverlay"></div>

    <div class="header w3-bar">
      <!--<button class="w3-button w3-teal w3-small" onclick="w3_open()">&#9776;</button>-->
      <button class="w3-bar-item w3-button" onclick="w3_open()" style="background-color: #08b0e8!important;"><i class="fa fa-bars"></i></button>
      <span class="w3-bar-item">{{ Config::get('app.name') }}<!----></span>
      <!--<h3>Chania</h3>
      <h1>Chania</h1>-->
    </div>

    @if(isset($child)) {!! $child !!}
    @else
    
    <div class="main" style="overflow-y: scroll;padding-bottom: 15px;">
       <div class="row">
        <div class="col-3 w3-col l12" style="padding-bottom: 0px"><!-- col-3 w3-col s12 m4 l2 -->
          <p><b>Welcome</b> Nama</p>
        </div>
      </div>
      <!---->
  	</div>

    @endif

    <div class="footer">
      <p>Login as Username, Kategori</p>
    </div>
    <form id="formcetak" name="formcetak" target="Map" method="POST" >
      <input type="hidden" id="termprint" name="termprint">
      <input type="hidden" id="additional" name="additional">
    </form>
    <script>
      function w3_open() {
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("myOverlay").style.display = "block";
      }

      function w3_close() {
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("myOverlay").style.display = "none";
      }
    </script>
    <script type="text/javascript">
      
      /*var availableTags = [
        "ActionScript",
        "AppleScript",
        "Asp",
        "BASIC",
        "C",
        "C++",
        "Clojure",
        "COBOL",
        "ColdFusion",
        "Erlang",
        "Fortran",
        "Groovy",
        "Haskell",
        "Java",
        "JavaScript",
        "Lisp",
        "Perl",
        "PHP",
        "Python",
        "Ruby",
        "Scala",
        "Scheme"
      ];
      $( "#autocomplete" ).autocomplete({
        source: availableTags
      });

      $( "#name" ).autocomplete({
        //http://127.0.0.1/default-theme/test/datasource.php?term=ja
        //source: "out-file/test/datasource.php",
        source: function(request, response) {
          jQuery.post("api/patient", {term: request.term}, response, 'json');
        },
        minLength: 2,
        select: function( event, ui ) {
          //log( "Selected: " + ui.item.value + " aka " + ui.item.id );
          //console.log(ui); console.log("Selected: " + ui.item.value + " aka " + ui.item.id);
          //$('#firstname').val(ui.item.id);
          s_patient(ui);
        }
      });
      function s_patient(ui) {
        $('#firstname').val(ui.item.id);
        $('#address').val(ui.item.address);
        $('#birthday').val(ui.item.birthday);
        $('#gender').val(ui.item.gender);
        $('#rmid').val(ui.item.rmid);
      }*/

      function cetaksmall() {
        //myWindow = window.open("testprint-small.html", "myWindow", "width=200,height=100");
        window.open("{{ URL::to('/') }}index.php/cetak/small", "Print Small 5x2", "width=600,height=250");
      }
      function gender(val) {
        if (val=='L') return 'Laki - laki';
        else return 'Perempuan';
      }

      var host = '{{ URL::to('/') }}';
    </script>
    <script src="{{ URL::to('/') }}/js/default.js"></script>
  </body>
</html>
