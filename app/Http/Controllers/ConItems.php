<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mod_items;
use App\Mod_lmitems;

class ConItems extends Controller {

	public function index (Request $req, $i="") {
		$prm = array(); //$arr = array();
		$prm['filter']['field'] = $req->input('field');
		$prm['filter']['term'] = $req->input('term');

		$row = 5;	//$link = "";
		$query = Mod_items::select('items.*')->orderBy('created_at', 'DESC'); //orderByRaw('updated_at - created_at DESC'); //->paginate($row); //Mod_items::all(); //Mod_items::select('email', 'name', 'type');
		if ($i) {
			$query = $query->where($prm['filter']['field'], 'LIKE', '%'.$prm['filter']['term'].'%');
			$query = $query->paginate($row); 
			$query->withPath('barang');
			$query->appends($prm['filter']);
		} else $query = $query->paginate($row);

		$prm['data'] = $query;
		if ($i) return view('childs.items.items_c', $prm);
		else {
			$prm['item_c'] = view('childs.items.items_c', $prm);
			$prm['child'] = view('childs.items', $prm);

			return view('index-', $prm);
		}/**/
	//return dd($prm);
	}
	public function itemupdate($req) {
		/*keyword: barangupdate, term: Item08052019184345, p-id: Item08052019184345, p-name: Nama4345*/
		//$keyword = $req->input('keyword');
		$res = array();
		$res['respon']['code'] = 2;
		$res['respon']['status'] = false;
		$term = $req->input('term');

		$update = array();
		$update['itemid'] = $req->input('p-id');
		$update['name'] = $req->input('p-name');

		$editid = ($update['itemid']!=$term) ? true : false;
		//$r = $r->get();
		//dd($r);
		//echo $r->quantity;

		if (Mod_items::find($update['itemid']) && $editid ) $res['respon']['desc'] = "Gagal, ".$update['itemid']." tersedia";
		else {
			$r = Mod_items::find($term);
			//echo $r->quantity;
			if ($editid) {
				$update['term'] = $req->input('p-id');
				$update['itemid'] = $term;
			}

			if ($status = Mod_items::i_update($update)) {
				$res['respon']['code'] = 4;
				$res['respon']['status'] = $status;
				$res['respon']['id'] = $update['itemid'];
				$res['respon']['desc'] = "Update berhasil";

				//jika term & p-id beda, maka insert ke log keterangan update
				//if ($update['itemid']!=$update['term']) { 
				if ($editid) {//echo "masuk logitem";
					//$r = Mod_items::find($update['itemid']);
					$insertlm = array();
					$insertlm['itemid'] = $update['term'];
					$insertlm['initamount'] = $r->quantity;
					$insertlm['finalamount'] = $r->quantity;
					$insertlm['currentamount'] = $r->quantity;
					$insertlm['description'] = $update['itemid']." Update mulai awal"; //var_dump($insertlm);				
					Mod_lmitems::lm_new($insertlm); //['itemid'=>'', 'initamount'=>'', 'finalamount'=>'', 'currentamount'=>'']
				} //else echo "tidak masuk logitem";

			} else $res['respon']['desc'] = "Gagal, ".$update['itemid']." tidak dapat diperbarui";

		}/**/ //else $res['respon']['desc'] = "Gagal, ".$update['itemid']." tersedia";

	return $res;
	}
	public function itemsave($req) {
		$res = array();
		$res['respon']['code'] = 2;
		$res['respon']['status'] = false;

		//patient p-name=Test+Name&p-address=Jl.+Mataram&p-birthday=18-01-2019&p-gender=L&p-rmid=00019041901
		$input = array();		
		$input['itemid'] = $req->input('p-id');
		$input['name'] = $req->input('p-name');
		$input['quantity'] = $req->input('p-quantity');
		
		if (Mod_items::find($input['itemid']) ) $res['respon']['desc'] = "Gagal, ".$input['itemid']." tersedia";
		else {
			$status = Mod_items::i_new($input);
			$res['respon']['code'] = 4;
			$res['respon']['status'] = $status;
			$res['respon']['id'] = $input['itemid'];
			$res['respon']['desc'] = "Update berhasil";

			$insertlm = array();
			$insertlm['itemid'] = $input['itemid'];
			$insertlm['initamount'] = 0;
			$insertlm['finalamount'] = $input['quantity'];
			$insertlm['currentamount'] = $input['quantity'];
			$insertlm['description'] = $input['itemid']." mulai baru"; //var_dump($insertlm);				
			Mod_lmitems::lm_new($insertlm);
		}

	return $res;
	}
	public function itemdelete($req) {
		$res = array();
		$res['respon']['code'] = 2;
		$res['respon']['status'] = false;

		//patient p-name=Test+Name&p-address=Jl.+Mataram&p-birthday=18-01-2019&p-gender=L&p-rmid=00019041901
		$delete = array();
		$delete['itemid'] = $req->input('term');
		
		$r = Mod_items::find($delete['itemid']);
		$del = Mod_items::i_delete($delete);
		if ($del) {
			//$status = Mod_items::i_new($delete);
			$res['respon']['code'] = 4;
			$res['respon']['status'] = $del;
			$res['respon']['id'] = $delete['itemid'];
			$res['respon']['desc'] = "Delete sukses";

			$insertlm = array();
			$insertlm['itemid'] = $delete['itemid'];
			$insertlm['initamount'] = $r->quantity;
			$insertlm['finalamount'] = $r->quantity;
			$insertlm['currentamount'] = $r->quantity;
			$insertlm['description'] = $delete['itemid']." dihapus"; //var_dump($insertlm);
			Mod_lmitems::lm_new($insertlm);
		} else $res['respon']['desc'] = "Tidak dapat dihapus";

	return $res;
	}	
  public function item_aut(Request $req) {
    $input = array();
		$input['term'] = $req->input('term');
    $temp = Mod_items::select('items.*')->where('name', 'LIKE', '%'.$input['term'].'%')->get();
    $data = array();
    foreach ($temp as $k => $v) {
      $data[] = array('label' => implode(" - ", array($v->name, $v->itemid)),
                      'value' => $v->name,
                      'id' => $v->itemid,
                      'barcode' => $v->itemid,
                      'quantity' => $v->quantity,
                      'create_at' => $v->created_at,
                      'update_at' => $v->updated_at);
    }

  return $data;
  }
}
