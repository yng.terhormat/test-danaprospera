<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mod_mutation;
use App\Mod_cmutation;
use App\Mod_items;
//use App\Mod_lmitems;

use DB;

class ConMutation extends Controller {

	public function index($value='') {
		//echo "helloworld";
		$prm = array();
		$prm['norecipe'] = $this->no_mutasi(); //date("His"); //$this->Mod_recipe->recipe_number();
		$prm['howtouse'] = ""; //$this->check('htu');
		$prm['infouse'] = ""; //$this->check('ifu');
		$prm['introuse'] = ""; //$this->check('itu');
		$prm['introinfo'] = ""; //$this->check('iti');
		$prm['fungsi'] = ""; //$this->fungsi;
		//$prm['childs'] = $this->load->view('childs/obat', $prm);
		//$this->load->view('index-', $prm);

		$prm['child'] = view('childs.mutation', $prm);

	return view('index-', $prm);
	}
	public function report(Request $req) {
		$prm = array();
		$prm['norecipe'] = ""; //$this->no_mutasi(); //date("His"); //$this->Mod_recipe->recipe_number();
		$prm['howtouse'] = ""; //$this->check('htu');
		$prm['infouse'] = ""; //$this->check('ifu');
		$prm['introuse'] = ""; //$this->check('itu');
		$prm['introinfo'] = ""; //$this->check('iti');
		$prm['fungsi'] = ""; //$this->fungsi;

		$prm['_tgla'] = ($req->input('tgla')) ? $req->input('tgla') : date("d-m-Y");
		$prm['_tglb'] = ($req->input('tglb')) ? $req->input('tglb') : date("d-m-Y");
		$tgla = $this->deffordate($prm['_tgla']);
		$tglb = $this->deffordate($prm['_tglb']);
		$mutation = Mod_mutation::with('items');
		//$mutation = $mutation->whereDay('created_at', date("d", $tgla));
		$mutation = $mutation->whereBetween('created_at', [date("Y-m-d 00:00:00", $tgla), date("Y-m-d 23:59:59", $tglb)]);//->whereBetween('votes', [1, 100])
		//echo $mutation->toSql();
		$prm['data'] = $mutation->get();
		//dd($prm['data']);
		/*$mut = Mod_mutation::with('items')->get(); //find('MT120307411');
		dd($mut);
		foreach ($mut as $key => $val) {
			echo $val->pivot->created_at;
		}*/
		$prm['child'] = view('childs.mutation.report', $prm);

	return view('index-', $prm);
	}
	private function deffordate($value='') {
		return mktime(0, 0, 0, substr($value, 3, 2 ), substr($value, 0, 2 ), substr($value, -4));
	}
	public function mutsave($req='') { //echo "mutsave";
		$res = array();
		$res['respon']['code'] = 2;
		$res['respon']['status'] = false;

		$mutasi = array();
		$mutasi['mutation']['mutationid'] = $req->input('nomutasi');
		$mutasi['mutation']['datemut_at'] = date("Y-m-d ", mktime(0, 0, 0, substr($req->input('datein'), 3, 2 ), substr($req->input('datein'), 0, 2 ), substr($req->input('datein'), -4)) ).date("H:i:s"); //var_dump($_POST);
		$i = 0;
		foreach ($_POST['forall'] as $key => $val) {
			$mutasi['cmutation'][$val]['mutationid'] = $req->input('nomutasi');
			$mutasi['cmutation'][$val]['itemid'] = $req->input('itemid-txt'.$val);
			
			//$req->input('tipe-txt'.$key) 1=>masuk(in), 2=>keluar(out)
			$mutasi['cmutation'][$val]['status'] = $req->input('tipe-txt'.$val);			
			$mutasi['cmutation'][$key]['in'] = ($req->input('tipe-txt'.$key)==1) ? $req->input('jumlah-txt'.$key) : 0;
			$mutasi['cmutation'][$key]['out'] = ($req->input('tipe-txt'.$key)==2) ? $req->input('jumlah-txt'.$key) : 0;;

			$i++;
		}
		$mutasi['mutation']['totalitem'] = $i;

		$trans = DB::transaction(function () use ($mutasi, $req, $res) {
			$mut = false;
			$mut = Mod_mutation::m_new($mutasi['mutation'], 1); //echo $mut;
			if ($mut) {
				foreach ($mutasi['cmutation'] as $key => $val) {

					$item = Mod_items::find($req->input('itemid-txt'.$key));
					$amount = ($req->input('tipe-txt'.$key)==1) ? ($item->quantity + $req->input('jumlah-txt'.$key)) : ($item->quantity - $req->input('jumlah-txt'.$key));
					$mutasi['items'][$key]['itemid'] = $req->input('itemid-txt'.$key);
					$mutasi['items'][$key]['quantity'] = $amount;

					Mod_items::i_update($mutasi['items'][$key]);
					Mod_cmutation::cm_new($val);

					/*for log
					//$c_item = Mod_items::find($req->input('itemid-txt'.$key));
					$mutasi['lmitems'][$key]['itemid'] = $req->input('itemid-txt'.$key);
					$mutasi['lmitems'][$key]['startamount'] = 31; //$item->quantity;
					$mutasi['lmitems'][$key]['in'] = ($req->input('tipe-txt'.$key)==1) ? $req->input('jumlah-txt'.$key) : 0;
					$mutasi['lmitems'][$key]['out'] = ($req->input('tipe-txt'.$key)==2) ? $req->input('jumlah-txt'.$key) : 0;;
					$mutasi['lmitems'][$key]['currentamount'] = 200; //$c_item->quantity;
					$mutasi['lmitems'][$key]['description'] = "From mutasi barang in:".$mutasi['lmitems'][$key]['in'].", out:".$mutasi['lmitems'][$key]['out'].", current:".$mutasi['lmitems'][$key]['currentamount'];
					//Mod_lmitems::lm_new($mutasi['lmitems'][$key]);*/
				}

				$res['code'] = 4;
				$res['status'] = $mut;
				$res['id'] = $mutasi['mutation']['mutationid'];
				$res['desc'] = "Mutasi barang no.".$res['id']." berhasil.";
			}

		return $res;
		});
		//{"respon":{"code":4,"status":false,"transaction":null}}
		if (is_array($trans)) {
			$res['respon']['code'] = $trans['code'];
			$res['respon']['status'] = $trans['status'];
			$res['respon']['id'] = $trans['id'];
			$res['respon']['desc'] = $trans['desc'];
		}/**/

	return $res; //$res; $mutasi;
	}
	public function mutupdate($req='') {
/*
array(13) {
  ["keyword"]=>
  string(9) "mutupdate"
  ["muttemp"]=>
  array(1) {
    [0]=>
    string(0) ""
  }
  ["datemut0"]=>
  string(10) "12-05-2019"
  ["mutationid0"]=>
  string(11) "MT120307411"
  ["itetemp"]=>
  array(1) {
    [0]=>
    string(0) ""
  }
  ["itemutid00"]=>
  string(1) "1"
  ["itemname00"]=>
  string(15) "Tes LogMutation"
  ["itempin00"]=>
  string(2) "12"
  ["itempout00"]=>
  string(1) "0"
  ["itemutid01"]=>
  string(1) "2"
  ["itemname01"]=>
  string(15) "Tes LogMutation"
  ["itempin01"]=>
  string(1) "0"
  ["itempout01"]=>
  string(2) "21"
}
*/
		$res = array();
		$res['respon']['code'] = 2;
		$res['respon']['status'] = false;
		$res['respon']['key'] = 0;
		$prm = array();
		foreach ($_POST['muttemp'] as $key => $val) {
			$prm['data']['mutasi'][$key]['mutationid'] = $req->input('mutationid'.$key);
			$prm['data']['mutasi'][$key]['datemut_at'] = date("Y-m-d H:i:s", $this->deffordate($req->input('datemut'.$key)));
			$res['respon']['id'] = $req->input('mutationid'.$key);

			foreach ($_POST['itetemp']  as $k => $v) {
				$prm['data']['itemut'][$k]['itemutid'] = $req->input('itemutid'.$key.$k);
				//$prm['data']['itemut'][$k]['itemname'] = $req->input('itemname'.$key.$k); //tidak bisa diedit karena ikut data barang
				$prm['data']['itemut'][$k]['in'] = $req->input('itempin'.$key.$k);
				$prm['data']['itemut'][$k]['out'] = $req->input('itempout'.$key.$k);
			}
		}
		
		$trans = DB::transaction(function () use ($prm, $req, $res) {
			foreach ($prm['data']['mutasi'] as $key => $value) Mod_mutation::m_update($value);
			foreach ($prm['data']['itemut'] as $key => $value) Mod_cmutation::cm_update($value);

			$res['code'] = 4;
			$res['status'] = true;
			$res['id'] = $res['respon']['id'];
			$res['desc'] = "Mutasi barang no.".$res['id']." berhasil.";
		return $res;
		});
		if (is_array($trans)) {
			$res['respon']['code'] = $trans['code'];
			$res['respon']['status'] = $trans['status'];
			$res['respon']['id'] = $trans['id'];
			$res['respon']['desc'] = $trans['desc'];
		}/**/
		//var_dump($prm);

	return $res;
	}
	public function mutdel($req='') {
		$res = array();
		$res['respon']['code'] = 2;
		$res['respon']['status'] = false;
		$prm = array();
		$prm['mutasi']['mutationid'] = $req->input('term');
		$res['respon']['status'] = true;

	return $res;
	}
	//->whereNotBetween('votes', [1, 100])
	private function no_mutasi() {
		$id = Mod_mutation::whereDay('created_at', date("d"))->count();
	return "MT".date("dHis").($id + 1);
	}
  public function autocom($req='') {
  	$prm = array();
    $prm['field'] = "mutationid";
    $prm['keyword'] = $req->input('term');
    $temp = Mod_mutation::where($prm['field'], 'like', '%'.$prm['keyword'].'%')->get();   
    $data = array();
    foreach ($temp as $k => $v) {
      $value = $v->mutationid; //($filter=='name') ? $v->name : $v->rmid;
      //$umur = $fungsi->datediff(date("Y-m-d H:i:s", strtotime($v->birthday)), date("Y-m-d H:i:s"));
      $data[] = array('label' => implode(" - ", array($v->mutationid, $v->datemut_at)),
                      'value' => $value, //$v->name,
                      'datemut_at' => date("d-m-Y", strtotime($v->datemut_at) ),
                      'totalitem' => $v->totalitem,
                      'id' => $v->mutationid,
                      'created_at' => $v->created_at,
                      'updated_at' => $v->updated_at);
    }/**/

  return $data;
  }

}
