<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConApi extends Controller {
private $ConItems;
private $ConMutation;
private $ConItemut;
private $req;

	public function __construct(ConItems $ConItems, ConMutation $ConMutation, ConItemut $ConItemut) {
		$this->ConItems = $ConItems;
		$this->ConMutation = $ConMutation;
		$this->ConItemut = $ConItemut;
	}

	public function test(Request $req) {
		return "Oke Tes";
	}

	public function barang(Request $req) {
		$keyword = $req->input('keyword');
		$this->req = $req;

		switch ($keyword) {
			case 'autocom':
				$res = $this->autocom();
			break;
			case 'itemdelete':
				return $this->ConItems->itemdelete($req);
			break;
			case 'itemsave':
				return $this->ConItems->itemsave($req);
			break;
			case 'itemupdate':
				return $this->ConItems->itemupdate($req);
			break;
			case 'itemsearch': //echo $keyword;
				return $this->ConItems->index($req, 1);
			break;
			default:
				$res = $this->fungsi->output('json', array('respon' => array('code' => 2, 'status' => 'unregistered')));
			break;
		}
	}

	public function mutasi(Request $req) {
		$keyword = $req->input('keyword');
		$this->req = $req;

		switch ($keyword) {
			case 'autocom':
				return $this->ConMutation->autocom($req);
			break;/**/
			case 'mutdel':
				return $this->ConMutation->mutdel($req);
			break;
			case 'mutsave':
				return $this->ConMutation->mutsave($req);
			break;
			case 'mutupdate':
				return $this->ConMutation->mutupdate($req);
			break;
			case 'mutlist': //echo $keyword;
				return $this->ConItemut->mutlist_($req);
			break;
			case 'imutupdate': //echo $keyword;
				return $this->ConItemut->imutupdate($req);
			break;/**/
			default:
				$res = array('respon' => array('code' => 2, 'status' => 'unregistered')); //$this->fungsi->output('json', array('respon' => array('code' => 2, 'status' => 'unregistered')));
			break;
		}
	}
	public function itemut(Request $req) {
		$keyword = $req->input('keyword');
		$this->req = $req;

		switch ($keyword) {
			case 'delitemut':
				return $this->ConItemut->delitemut($req);
			break;
			default:
				$res = array('respon' => array('code' => 2, 'status' => 'unregistered')); //$this->fungsi->output('json', array('respon' => array('code' => 2, 'status' => 'unregistered')));
			break;
		}
	}
}
