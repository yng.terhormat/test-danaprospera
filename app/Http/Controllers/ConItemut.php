<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mod_mutation;
use App\Mod_cmutation;
use App\Mod_items;

use DB;

class ConItemut extends Controller {

  public function mutlist_($req='') {
  	$prm = array();
  	$prm['field'] = "mutationid";
  	$prm['keyword'] = $req->input('term');

  	$data = Mod_cmutation::where($prm['field'], $prm['keyword'])
				  	->leftJoin('items', 'itemut.itemid', '=', 'items.itemid')
				  	->get();

  return $data;
  }
  public function imutupdate($req='') {
  	//var_dump($_POST);
  	//if (itemim-txt3) insert baru
		$res = array();
		$res['respon']['code'] = 2;
		$res['respon']['status'] = false;

		$mutasi = array();
		$mutasi['mutation']['mutationid'] = $req->input('nomutasi');
		$mutasi['mutation']['datemut_at'] = date("Y-m-d ", mktime(0, 0, 0, substr($req->input('datein'), 3, 2 ), substr($req->input('datein'), 0, 2 ), substr($req->input('datein'), -4)) ).date("H:i:s"); //var_dump($_POST);
		$i = 0;
		foreach ($_POST['forall'] as $key => $val) {
			$mutasi['cmutation'][$val]['mutationid'] = $req->input('nomutasi');
			$mutasi['cmutation'][$val]['itemid'] = $req->input('itemid-txt'.$val);
			
			//$req->input('tipe-txt'.$key) 1=>masuk(in), 2=>keluar(out)
			$mutasi['cmutation'][$val]['itemutid'] = $req->input('itemim-txt'.$val);
			$mutasi['cmutation'][$val]['status'] = $req->input('tipe-txt'.$val);
			$mutasi['cmutation'][$key]['in'] = ($req->input('tipe-txt'.$key)==1) ? $req->input('jumlah-txt'.$key) : 0;
			$mutasi['cmutation'][$key]['out'] = ($req->input('tipe-txt'.$key)==2) ? $req->input('jumlah-txt'.$key) : 0;;

			$i++;
		}
		$mutasi['mutation']['totalitem'] = $i;
  	//var_dump($mutasi['mutation']);
		$trans = DB::transaction(function () use ($mutasi, $req, $res) {
			$mut = false;
			$mut = Mod_mutation::m_update($mutasi['mutation']); //Mod_mutation::m_new($mutasi['mutation'], 1); //echo $mut;
			if ($mut) {
				foreach ($mutasi['cmutation'] as $key => $val) {
					/*$item = Mod_items::find($req->input('itemid-txt'.$key));
					$amount = ($req->input('tipe-txt'.$key)==1) ? ($item->quantity + $req->input('jumlah-txt'.$key)) : ($item->quantity - $req->input('jumlah-txt'.$key));
					$mutasi['items'][$key]['itemid'] = $req->input('itemid-txt'.$key);
					$mutasi['items'][$key]['quantity'] = $amount;
					Mod_items::i_update($mutasi['items'][$key]);*/

					//Mod_cmutation::cm_new($val);
					if ($val['itemutid']) {
						//var_dump($val);
						//echo "updated<br>";
						//unset($val['itemutid']);
						Mod_cmutation::cm_update($val);
					} else {
						//echo "new data itemut<br>";
						unset($val['itemutid']);
						Mod_cmutation::cm_new($val);
					}
				}

				$res['code'] = 4;
				$res['status'] = $mut;
				$res['id'] = $mutasi['mutation']['mutationid'];
				$res['desc'] = "Update Mutasi barang no.".$res['id']." berhasil.";
			}

		return $res;
		});

		//{"respon":{"code":4,"status":false,"transaction":null}}
		if (is_array($trans)) {
			$res['respon']['code'] = $trans['code'];
			$res['respon']['status'] = $trans['status'];
			$res['respon']['id'] = $trans['id'];
			$res['respon']['desc'] = $trans['desc'];
		}/**/

	return $res; //$res; $mutasi;
  }
  public function delitemut($req='') {
  	$res = array();
  	$prm = array();
  	//var_dump($_POST);

  	$prm['itemutid'] = $req->input('term');
  	$del = Mod_cmutation::cm_delete($prm);

		$res['respon']['code'] = 4; //$trans['code'];
		$res['respon']['status'] = $del;
		$res['respon']['id'] = $prm['itemutid'];
		$res['respon']['desc'] = "Data ".$prm['itemutid']." telah dihapus";

	return $res;
  }
}
