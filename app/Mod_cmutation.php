<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mod_cmutation extends Model {
	protected $table = "itemut";
	protected $primaryKey = "itemutid";
  use SoftDeletes;

	//return $this->hasMany('App\Comment', 'foreign_key', 'local_key');
  /*public function mutation() {
  	return $this->belongsTo('App\Mod_mutation', 'mutationid', 'mutationid');
  }
	public function item() {
	  return $this->hasOne('App\Mod_items', 'itemid', 'itemid');
	}*/

  public static function cm_new($prm, $i=1) {//var_dump($prm);
    $cm_new = new static;
    foreach ($prm as $key => $val) $cm_new->$key = $val;
    $res = $cm_new->save();

    if ($i) $res = $cm_new->itemutid;

  return $res;
  }
  public static function cm_update($prm) { //var_dump($prm);
    $cm_update = static::find($prm['itemutid']);
    foreach ($prm as $key => $val) {
      if ($key!="itemutid") $cm_update->$key = $val;
    }

  return $cm_update->save();
  }
  public static function cm_delete($prm) {
  	$res = false;
  	if (array_key_exists("itemutid", $prm)) {
			$res = static::where('itemutid', $prm['itemutid'])->delete();  		
  	}
  return $res;
  }
  public static function cm_select($prm) {
    $res = static::select('items.*'); //items //self::$table
    echo $res->toSql();
    
  return $res;
  }
	public static function cm_get($prm="") {
		if (!is_array($prm)) {
			$res = (empty($prm)) ? static::whereIn('status', [1, 2]) : static::where('status', (int) $prm);
			$res = $res->get(); //->toArray();
		} else {
			$field = array('itemutid', 'status'); //$cars = false;
			foreach ($prm as $key => $val) {
				if (in_array($key, $field)) {
					if (!empty($res)) $res = $res->where($key, $val);
					else $res = static::where($key, $val);
				} else {
					$res = false;
					break;
				}
			}
			//echo query toSql() here, cos other place ERROR
			$res = ($res) ? $res->get() : $res; //static::find($prm['itemutid'])->first();
		}

	return $res;
	}	
}
