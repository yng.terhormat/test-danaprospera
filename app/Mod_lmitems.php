<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mod_lmitems extends Model {
	protected $table = "logmutationitem";
	protected $primaryKey = "id";
	//public $timestamps = false;
	const UPDATED_AT = null;

	//return $this->hasMany('App\Comment', 'foreign_key', 'local_key');
  public function items() {
  	return $this->belongsTo('App\Mod_items', 'itemid', 'id');
  }

  public static function lm_new($prm, $i=1) {
    $lm_new = new static;
    foreach ($prm as $key => $val) $lm_new->$key = $val;
    $res = $lm_new->save();

    if ($i) $res = $lm_new->id;

  return $res;
  }
  public static function lm_update($prm) {
    $lm_update = static::find($prm['itemid']);
    foreach ($prm as $key => $val) {
      if ($key!=$this->primaryKey) $lm_update->$key = $val;
    }

  return $lm_update->save();
  }
  /*public static function i_delete($prm) {
  	$res = false;
  	if (array_key_exists("itemid", $prm)) {
			$res = static::where('itemid', $prm['itemid'])->delete();  		
  	}
  return $res;
  }*/
  public static function lm_select($prm) {
    $res = static::select('items.*'); //items //self::$table
    //echo $res->toSql();
    
  return $res;
  }
	public static function lm_get($prm="") {
		if (!is_array($prm)) {
			$res = (empty($prm)) ? static::whereIn('status', [1, 2]) : static::where('status', (int) $prm);
			$res = $res->get(); //->toArray();
		} else {
			$field = array('itemid', 'status'); //$cars = false;
			foreach ($prm as $key => $val) {
				if (in_array($key, $field)) {
					if (!empty($res)) $res = $res->where($key, $val);
					else $res = static::where($key, $val);
				} else {
					$res = false;
					break;
				}
			}
			//echo query toSql() here, cos other place ERROR
			$res = ($res) ? $res->get() : $res; //static::find($prm['itemid'])->first();
		}

	return $res;
	}

}
