<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mod_items extends Model {
	protected $table = "items"; 
	protected $primaryKey = "itemid";
  public $incrementing = false; //if primary key not like default (int) id
	use SoftDeletes;

	//return $this->hasMany('App\Comment', 'foreign_key', 'local_key');
  /*public function log() {
  	return $this->hasMany('App\Mod_lmitems', 'itemid', 'itemid');
  }
  public function cmutation() {
    return $this->hasOne('App\Mod_cmutation');
  }*/
  public function mutation() {
    //return $this->belongsToMany('App\Mod_mutation', 'itemut', 'mutationid', 'itemid');
    return $this->belongsToMany('App\Mod_mutation');//->withPivot('mutationid', 'itemid');
  }

  public static function i_new($prm, $i=1) {
    $b_new = new static;
    foreach ($prm as $key => $val) $b_new->$key = $val;
    $res = $b_new->save();

    if ($i) $res = $b_new->itemid;

  return $res;
  }
  public static function i_update($prm) {
    $i_update = static::find($prm['itemid']);
    foreach ($prm as $key => $val) {
      if ($key!="itemid" && $key!="term") $i_update->$key = $val;
      if ($key=="term") $i_update->itemid = $val;
    }

  return $i_update->save();
  }
  public static function i_delete($prm) {
  	$res = false;
  	if (array_key_exists("itemid", $prm)) {
			$res = static::where('itemid', $prm['itemid'])->delete();  		
  	}
  return $res;
  }
  public static function i_select($prm) {
    $res = static::select('items.*'); //items //self::$table
    echo $res->toSql();
    
  return $res;
  }
	public static function i_get($prm="") {
		if (!is_array($prm)) {
			$res = (empty($prm)) ? static::whereIn('status', [1, 2]) : static::where('status', (int) $prm);
			$res = $res->get(); //->toArray();
		} else {
			$field = array('itemid', 'status'); //$cars = false;
			foreach ($prm as $key => $val) {
				if (in_array($key, $field)) {
					if (!empty($res)) $res = $res->where($key, $val);
					else $res = static::where($key, $val);
				} else {
					$res = false;
					break;
				}
			}
			//echo query toSql() here, cos other place ERROR
			$res = ($res) ? $res->get() : $res; //static::find($prm['itemid'])->first();
		}

	return $res;
	}
}
