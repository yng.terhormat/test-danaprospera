<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Mod_mutation extends Model { 
	protected $table = "mutations";
	protected $primaryKey = "mutationid";
  public $incrementing = false; //if primary key not like default (int) id
	use SoftDeletes;

	//return $this->hasMany('App\Comment', 'foreign_key', 'local_key');
  public function items() {
  	return $this->belongsToMany('App\Mod_items', 'itemut', 'mutationid', 'itemid')->withPivot('itemutid', 'in', 'out', 'status', 'created_at', 'updated_at');//, 'mutationid', 'mutationid'
    //return $this->belongsToMany('App\Mod_items');
  }/**/

  public static function m_new($prm, $i=1) {
    $m_new = new static;
    foreach ($prm as $key => $val) $m_new->$key = $val;

    $res = $m_new->save();
    if ($i) $res = $m_new->mutationid;

  return $res;
  }
  public static function m_update($prm) {
    //var_dump($prm);
    $m_update = static::find($prm['mutationid']);
    foreach ($prm as $key => $val) {
      if ($key!="mutationid") $m_update->$key = $val;
    }

  return $m_update->save();
  }
  public static function m_delete($prm) {
  	$res = false;
  	if (array_key_exists("mutationid", $prm)) {
			$res = static::where('mutationid', $prm['mutationid'])->delete();  		
  	}
  return $res;
  }
  public static function m_select($prm="") {
    $res = static::select('items.*'); //items //self::$table
    //echo $res->toSql();
    
  return $res;
  }
	public static function m_get($prm="") {
		if (!is_array($prm)) {
			$res = (empty($prm)) ? static::whereIn('status', [1, 2]) : static::where('status', (int) $prm);
			$res = $res->get(); //->toArray();
		} else {
			$field = array('mutationid', 'status'); //$cars = false;
			foreach ($prm as $key => $val) {
				if (in_array($key, $field)) {
					if (!empty($res)) $res = $res->where($key, $val);
					else $res = static::where($key, $val);
				} else {
					$res = false;
					break;
				}
			}
			//echo query toSql() here, cos other place ERROR
			$res = ($res) ? $res->get() : $res; //static::find($prm['mutationid'])->first();
		}

	return $res;
	}

}
